package com.upbit;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.upbit.dto.TickerDto;

public class GetCandlesMinutes implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    String serverUrl = Main.serverUrl;

    HashMap<String, String> params = new HashMap<String, String>();
    // multiple "KRW-BTT,KRW-BTC"
    params.put("markets", "KRW-BTT");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(serverUrl + "/v1/ticker?" + queryString);
        request.setHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        String resJsonString = EntityUtils.toString(entity, "UTF-8");

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<TickerDto>>() {}.getType();
        ArrayList<TickerDto> data = gson.fromJson(resJsonString, type);

        System.out.println(data.get(0).getTrade_price());
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

}
