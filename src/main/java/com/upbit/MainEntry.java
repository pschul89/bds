package com.upbit;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import com.upbit.volatilityBreakOut.JobBid;
import com.upbit.volatilityBreakOut.JobSetting;

public class MainEntry {

  public static void main(String[] args) {
//    PropertyConfigurator.configure("log4j.properties");

//    new JobSetting(15);
    SchedulerFactory schedulerFactory = new StdSchedulerFactory();

    try {
        Scheduler scheduler = schedulerFactory.getScheduler();

        JobDetail jobVolatilityBreakOut = newJob(JobBid.class)
            .withIdentity("jobName", Scheduler.DEFAULT_GROUP)
            .build();

        Trigger trigger = newTrigger()
            .withIdentity("trggerName", Scheduler.DEFAULT_GROUP)
            .withSchedule(cronSchedule("0/5 * * * * ?"))
            .build();

        scheduler.scheduleJob(jobVolatilityBreakOut, trigger);
        scheduler.start();
    } catch(Exception e) {
        e.printStackTrace();
    }
  }
}
