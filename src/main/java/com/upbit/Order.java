package com.upbit;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;

public class Order {

  public void order(String market, String side) {
    if (null == side || "".equals(side)) {
      side = "bid";
    }
    String accessKey = Main.accessKey;
    String secretKey = Main.secretKey;
    String serverUrl = Main.serverUrl;

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", market);
    params.put("side", side);
    params.put("volume", "1000");
    params.put("price", "5");
    params.put("ord_type", "limit");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }

    String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

    Algorithm algorithm = Algorithm.HMAC256(secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .withClaim("query_hash", queryHash)
            .withClaim("query_hash_alg", "SHA512")
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(serverUrl + "/v1/orders");
        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);
        request.setEntity(new StringEntity(new Gson().toJson(params)));

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity, "UTF-8"));
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

  public void cancel() {
    String accessKey = Main.accessKey;
    String secretKey = Main.secretKey;
    String serverUrl = Main.serverUrl;

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", "KRW-BTT");
    params.put("side", "bid");
    params.put("volume", "1000");
    params.put("price", "5");
    params.put("ord_type", "limit");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }

    String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

    Algorithm algorithm = Algorithm.HMAC256(secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .withClaim("query_hash", queryHash)
            .withClaim("query_hash_alg", "SHA512")
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(serverUrl + "/v1/orders");
        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);
        request.setEntity(new StringEntity(new Gson().toJson(params)));

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity, "UTF-8"));
    } catch (IOException e) {
        e.printStackTrace();
    }
  }
}
