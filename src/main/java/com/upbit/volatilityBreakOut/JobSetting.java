package com.upbit.volatilityBreakOut;

import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.upbit.api.Accounts;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;

public class JobSetting implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    try {
      // 자산조회
      List<AccountsDto> accountsList = new Accounts().get();
      AccountsDto krwAccountsDto = accountsList.stream().filter(f -> "KRW".equals(f.getCurrency())).findAny().orElse(null);
      if(krwAccountsDto == null) {
        System.out.println("자산이 없습니다. [KRW]");
        return;
      } else {
        StaticConfig.balanceKrw = Double.parseDouble(krwAccountsDto.getBalance());
      }

      // 비트토렌트 존재여부
      boolean isBtt = accountsList.stream().anyMatch(m -> Main.market.equals(m.getCurrency()));

      StaticConfig.isBiding = !isBtt;
      StaticConfig.isAsking = isBtt;

      System.out.println("[current setting] 수익률: " + StaticConfig.yield);
      System.out.println("[current setting] 현재 KRW 자산: " + StaticConfig.balanceKrw);
      System.out.println("[current setting] 매수중 여부: " + StaticConfig.isBiding);
      System.out.println("[current setting] 매도중 여부: " + StaticConfig.isAsking);
      System.out.println("[current setting] 매수금액 설정: " + StaticConfig.bidAmt);
      System.out.println("[current setting] 매도금액 설정: " + StaticConfig.askAmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
