package com.upbit.volatilityBreakOut;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.upbit.api.Accounts;
import com.upbit.api.GetOrders;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;
import com.upbit.dto.GetOrdersDto;
import com.upbit.sc.MainSc;

public class TestMain {
  public static void main(String[] args) throws ClientProtocolException, IOException, NoSuchAlgorithmException {
    double bidAmt = 608.5;
    double susuryo = 9602 * 0.0005;
    double fullBidAmt = 9602 - susuryo;

    int volume = (int) (fullBidAmt / 608.5);
//    new Orders().post("KRW-" + Main.market, "bid", volume + "", bidAmt + "", "limit");

    double minAmt = 1;
    double minAmt1 = 0.1;
    double minAmt2 = 0.01;

//    OrdersChanceDto ordersChance = new OrdersChance().get();
//    StaticConfig.minTotal = ordersChance.getMarket().getBid().getMin_total();
//    System.out.println("[current setting] 최소 매도/매수 금액: " + StaticConfig.minTotal);

//    List<AccountsDto> accountsList = new Accounts().get();
//    System.out.println(accountsList.toString());

//    System.out.println(1000 * (1 + (double)3 / 100));
    System.out.println(1 + MainSc.yield / 100);
    double temp = Double.parseDouble("622") * (1 + MainSc.yield / 100);
    double tradePrice = 624;
    if(tradePrice < 10) { // 0.01
      temp = Math.floor(temp * 100) / 100;
    } else if(tradePrice < 100) { // 0.1
      temp = Math.floor(temp * 10) / 10;
    } else if(tradePrice < 1000) { // 1
      temp = Math.floor(temp);
    } else if(tradePrice < 10000) { // 5
      temp = Math.floor(temp);
      if(temp % 10 > 5) {
        temp = temp + (5 - temp % 10);
      } else if(tradePrice % 10 > 0) {
        temp = temp - tradePrice % 10;
      }
    } else if(tradePrice < 100000) { // 10
      temp = Math.floor(temp / 10) * 10;
    } else if(tradePrice < 1000000) { // 50
      temp = Math.floor(temp / 10) * 10;
      if(temp % 100 > 50) {
        StaticConfig.bidAmt = temp + (50 - temp % 100);
      } else if(tradePrice % 100 > 0) {
        StaticConfig.bidAmt = temp - tradePrice % 100;
      } else {
        StaticConfig.bidAmt = temp;
      }
    } else {
      System.out.println("현재가가 너무 높은 코인입니다. >= 1000000");
      return;
    }
    System.out.println("temp: " + temp);


    double aa = 92234.21421421;

    System.out.println(Math.floor(aa * 100) / 100); // 0.01
    System.out.println(Math.floor(aa * 10) / 10); // 0.1
    System.out.println(Math.floor(aa)); // 1
    System.out.println(Math.floor(aa / 10) * 10); // 10
    System.out.println(Math.floor(aa / 10) * 10); // 50

    List<GetOrdersDto> getOrders = new GetOrders().get();
    System.out.println(getOrders.toString());
    List<GetOrdersDto> getOrdersMarket = getOrders.stream().filter(f -> MainSc.market.equals(f.getMarket())).collect(Collectors.toList());
  }
}
