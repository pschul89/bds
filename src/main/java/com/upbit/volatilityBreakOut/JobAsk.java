package com.upbit.volatilityBreakOut;

import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.upbit.api.Accounts;
import com.upbit.api.Orders;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;

public class JobAsk implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    try {
      if(StaticConfig.isAsking) {
        // 자산조회
        List<AccountsDto> accountsList = new Accounts().get();
        System.out.println(accountsList.toString());

        AccountsDto bttAccountsDto = accountsList.stream().filter(f -> Main.market.equals(f.getCurrency())).findAny().orElse(null);
        if(bttAccountsDto == null) {
          System.out.println("자산이 없습니다. [" + Main.market + "]");
          return;
        } else {
          // 시장가 매도
          new Orders().post("KRW-" + Main.market, "ask", bttAccountsDto.getBalance(), null, "market");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
