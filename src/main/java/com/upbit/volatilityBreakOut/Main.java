package com.upbit.volatilityBreakOut;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import java.util.List;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import com.upbit.api.Accounts;
import com.upbit.api.OrdersChance;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;
import com.upbit.dto.OrdersChanceDto;

public class Main {

  public static String market = "DOGE";
  public static int minuteUnit = 15;

  public static void main(String[] args) {
    try {
      // 자산조회
      List<AccountsDto> accountsList = new Accounts().get();
      AccountsDto krwAccountsDto = accountsList.stream().filter(f -> "KRW".equals(f.getCurrency())).findAny().orElse(null);
      if(krwAccountsDto == null) {
        System.out.println("자산이 없습니다. [KRW]");
        return;
      } else {
        StaticConfig.balanceKrw = Double.parseDouble(krwAccountsDto.getBalance());
      }

      // 비트토렌트 존재여부
      boolean isBtt = accountsList.stream().anyMatch(m -> market.equals(m.getCurrency()));

      StaticConfig.isBiding = !isBtt;
      StaticConfig.isAsking = isBtt;

      System.out.println("[current setting] 설정코인: " + market);
      System.out.println("[current setting] 수익률: " + StaticConfig.yield);
      System.out.println("[current setting] 현재 KRW 자산: " + StaticConfig.balanceKrw);
      System.out.println("[current setting] 매수중 여부: " + StaticConfig.isBiding);
      System.out.println("[current setting] 매도중 여부: " + StaticConfig.isAsking);
      System.out.println("[current setting] 매수금액 설정: " + StaticConfig.bidAmt);
      System.out.println("[current setting] 매도금액 설정: " + StaticConfig.askAmt);

      // 코인 최소 매도/매수 금액
      OrdersChanceDto ordersChance = new OrdersChance().get();
      StaticConfig.minTotal = ordersChance.getMarket().getBid().getMin_total();
      System.out.println("[current setting] 최소 매도/매수 금액: " + StaticConfig.minTotal);

      JobDetail jobSetting = newJob(JobSetting.class).withIdentity("jobSetting", Scheduler.DEFAULT_GROUP).build();
      JobDetail jobBid = newJob(JobBid.class).withIdentity("jobBid", Scheduler.DEFAULT_GROUP).build();
      JobDetail jobAsk = newJob(JobAsk.class).withIdentity("jobAsk", Scheduler.DEFAULT_GROUP).build();

      Trigger triggerMinuteUnit3s = newTrigger().withIdentity("trigger" + minuteUnit + "m1s", Scheduler.DEFAULT_GROUP).withSchedule(cronSchedule("1 0/" + minuteUnit + " * * * ?")).build();
      Trigger triggerMinuteUnit1s = newTrigger().withIdentity("trigger" + minuteUnit + "m3s", Scheduler.DEFAULT_GROUP).withSchedule(cronSchedule("3 0/" + minuteUnit + " * * * ?")).build();
      Trigger trigger1s = newTrigger().withIdentity("trigger1s", Scheduler.DEFAULT_GROUP).withSchedule(cronSchedule("* * * * * ?")).build();

      SchedulerFactory schedulerFactory1 = new StdSchedulerFactory();
      Scheduler scheduler1 = schedulerFactory1.getScheduler();
      scheduler1.scheduleJob(jobSetting, triggerMinuteUnit3s);

      SchedulerFactory schedulerFactory2 = new StdSchedulerFactory();
      Scheduler scheduler2 = schedulerFactory2.getScheduler();
      scheduler2.scheduleJob(jobBid, trigger1s);

      SchedulerFactory schedulerFactory3 = new StdSchedulerFactory();
      Scheduler scheduler3 = schedulerFactory3.getScheduler();
      scheduler3.scheduleJob(jobAsk, triggerMinuteUnit1s);

      scheduler1.start();
      scheduler2.start();
      scheduler3.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
