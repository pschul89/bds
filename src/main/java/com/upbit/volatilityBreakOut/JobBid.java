package com.upbit.volatilityBreakOut;

import java.util.ArrayList;
import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.upbit.api.CandlesMinutes;
import com.upbit.api.Orders;
import com.upbit.api.Ticker;
import com.upbit.config.StaticConfig;
import com.upbit.dto.CandlesMinutesDto;
import com.upbit.dto.TickerDto;

public class JobBid implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    try {
      if(StaticConfig.isBiding) {
        // 30분봉 정보
        ArrayList<CandlesMinutesDto> candlesMinutesList = new CandlesMinutes().get(Main.market, Main.minuteUnit);

        // 매수가격 책정 (시작가 + (고가-저가) * 0.5)
        StaticConfig.bidAmt = candlesMinutesList.get(0).getOpening_price() + ((candlesMinutesList.get(1).getHigh_price() - candlesMinutesList.get(1).getLow_price()) * 0.5);

        // 매수량
        // 자산 - 수수료 금액 = 총매수할수 있는 금액
        double susuryo = StaticConfig.balanceKrw * 0.0005;
        double fullBidAmt = StaticConfig.balanceKrw - susuryo;

        int volume = (int) (fullBidAmt / StaticConfig.bidAmt);

        List<TickerDto> tickerList = new Ticker().get(Main.market);

        System.out.println("설정된 매수금액: " + StaticConfig.bidAmt + ", 현재가: " + tickerList.get(0).getTrade_price() + ", 매수신호:" + (tickerList.get(0).getTrade_price() > StaticConfig.bidAmt));
        if(tickerList.get(0).getTrade_price() > StaticConfig.bidAmt) {
          new Orders().post("KRW-" + Main.market, "bid", volume + "", StaticConfig.bidAmt + "", "limit");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
