package com.upbit;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class Main {
  static String  accessKey = "XexSEQMhD09ifvmrwe4RePW2kR3P44fCKrIYprxb";
  static String secretKey = "RsXZmeZBxSQeCzf1IxcMeyxxiG4IfvdKFRuKt3Y9";
  static String serverUrl = "http://api.upbit.com";


  public static void main(String[] args) {
    getAccount();
    getOrdersChance();

    new Order().order("KRW-BTT", null);
  }

  public static void getAccount() {
    System.out.println(serverUrl + "/v1/accounts");
//    if(1==1) return;
    Algorithm algorithm = Algorithm.HMAC256(secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    System.out.println(authenticationToken);

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(serverUrl + "/v1/accounts");
//        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity, "UTF-8"));
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

  public static void getOrdersChance() {
    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", "KRW-BTC");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }

    String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

    Algorithm algorithm = Algorithm.HMAC256(secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .withClaim("query_hash", queryHash)
            .withClaim("query_hash_alg", "SHA512")
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(serverUrl + "/v1/orders/chance?" + queryString);
        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity, "UTF-8"));
    } catch (IOException e) {
        e.printStackTrace();
    }
  }
}
