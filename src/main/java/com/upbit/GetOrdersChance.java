package com.upbit;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class GetOrdersChance implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    String accessKey = Main.accessKey;
    String secretKey = Main.secretKey;
    String serverUrl = Main.serverUrl;

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", "KRW-BTT");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

    Algorithm algorithm = Algorithm.HMAC256(secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .withClaim("query_hash", queryHash)
            .withClaim("query_hash_alg", "SHA512")
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(serverUrl + "/v1/orders/chance?" + queryString);
        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity, "UTF-8"));
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

}
