package com.upbit.dto;

public class TickerDto {

  double trade_price;

  public double getTrade_price() {
    return trade_price;
  }

  public void setTrade_price(double trade_price) {
    this.trade_price = trade_price;
  }
}
