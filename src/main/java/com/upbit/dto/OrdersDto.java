package com.upbit.dto;

public class OrdersDto {

  String uuid;
  String side;
  String ord_type;
  String price;
  String state;
  String market;
  String created_at;
  String volume;
  String remaining_volume;
  String reserved_fee;
  String remaing_fee;
  String paid_fee;
  String locked;
  String executed_volume;
  int trade_count;
  public String getUuid() {
    return uuid;
  }
  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
  public String getSide() {
    return side;
  }
  public void setSide(String side) {
    this.side = side;
  }
  public String getOrd_type() {
    return ord_type;
  }
  public void setOrd_type(String ord_type) {
    this.ord_type = ord_type;
  }
  public String getPrice() {
    return price;
  }
  public void setPrice(String price) {
    this.price = price;
  }
  public String getState() {
    return state;
  }
  public void setState(String state) {
    this.state = state;
  }
  public String getMarket() {
    return market;
  }
  public void setMarket(String market) {
    this.market = market;
  }
  public String getCreated_at() {
    return created_at;
  }
  public void setCreated_at(String created_at) {
    this.created_at = created_at;
  }
  public String getVolume() {
    return volume;
  }
  public void setVolume(String volume) {
    this.volume = volume;
  }
  public String getRemaining_volume() {
    return remaining_volume;
  }
  public void setRemaining_volume(String remaining_volume) {
    this.remaining_volume = remaining_volume;
  }
  public String getReserved_fee() {
    return reserved_fee;
  }
  public void setReserved_fee(String reserved_fee) {
    this.reserved_fee = reserved_fee;
  }
  public String getRemaing_fee() {
    return remaing_fee;
  }
  public void setRemaing_fee(String remaing_fee) {
    this.remaing_fee = remaing_fee;
  }
  public String getPaid_fee() {
    return paid_fee;
  }
  public void setPaid_fee(String paid_fee) {
    this.paid_fee = paid_fee;
  }
  public String getLocked() {
    return locked;
  }
  public void setLocked(String locked) {
    this.locked = locked;
  }
  public String getExecuted_volume() {
    return executed_volume;
  }
  public void setExecuted_volume(String executed_volume) {
    this.executed_volume = executed_volume;
  }
  public int getTrade_count() {
    return trade_count;
  }
  public void setTrade_count(int trade_count) {
    this.trade_count = trade_count;
  }
  @Override
  public String toString() {
    return "OrdersDto [uuid=" + uuid + ", side=" + side + ", ord_type=" + ord_type + ", price=" + price + ", state=" + state + ", market=" + market
        + ", created_at=" + created_at + ", volume=" + volume + ", remaining_volume=" + remaining_volume + ", reserved_fee=" + reserved_fee
        + ", remaing_fee=" + remaing_fee + ", paid_fee=" + paid_fee + ", locked=" + locked + ", executed_volume=" + executed_volume + ", trade_count="
        + trade_count + "]";
  }

}
