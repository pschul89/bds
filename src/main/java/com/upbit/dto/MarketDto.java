package com.upbit.dto;

public class MarketDto {

  BidDto bid;

  public BidDto getBid() {
    return bid;
  }

  public void setBid(BidDto bid) {
    this.bid = bid;
  }

}
