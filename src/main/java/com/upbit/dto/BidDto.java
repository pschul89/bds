package com.upbit.dto;

public class BidDto {

  Double min_total;

  public Double getMin_total() {
    return min_total;
  }

  public void setMin_total(Double min_total) {
    this.min_total = min_total;
  }

}
