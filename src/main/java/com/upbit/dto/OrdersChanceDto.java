package com.upbit.dto;

public class OrdersChanceDto {

  MarketDto market;

  public MarketDto getMarket() {
    return market;
  }

  public void setMarket(MarketDto market) {
    this.market = market;
  }
}
