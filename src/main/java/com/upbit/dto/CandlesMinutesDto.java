package com.upbit.dto;

public class CandlesMinutesDto {

  String market; // 마켓명
  String candle_date_time_utc; // 캔들 기준 시각(UTC 기준)
  String candle_date_time_kst; // 캔들 기준 시각(KST 기준)
  double opening_price; // 시가
  double high_price; // 고가
  double low_price; // 저가
  double trade_price; // 종가
  long timestamp; // 해당 캔들에서 마지막 틱이 저장된 시각
  double candle_acc_trade_price; // 누적 거래 금액
  double candle_acc_trade_volume; // 누적 거래량
  int unit; // 분 단위(유닛)
  public String getMarket() {
    return market;
  }
  public void setMarket(String market) {
    this.market = market;
  }
  public String getCandle_date_time_utc() {
    return candle_date_time_utc;
  }
  public void setCandle_date_time_utc(String candle_date_time_utc) {
    this.candle_date_time_utc = candle_date_time_utc;
  }
  public String getCandle_date_time_kst() {
    return candle_date_time_kst;
  }
  public void setCandle_date_time_kst(String candle_date_time_kst) {
    this.candle_date_time_kst = candle_date_time_kst;
  }
  public double getOpening_price() {
    return opening_price;
  }
  public void setOpening_price(double opening_price) {
    this.opening_price = opening_price;
  }
  public double getHigh_price() {
    return high_price;
  }
  public void setHigh_price(double high_price) {
    this.high_price = high_price;
  }
  public double getLow_price() {
    return low_price;
  }
  public void setLow_price(double low_price) {
    this.low_price = low_price;
  }
  public double getTrade_price() {
    return trade_price;
  }
  public void setTrade_price(double trade_price) {
    this.trade_price = trade_price;
  }
  public long getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }
  public double getCandle_acc_trade_price() {
    return candle_acc_trade_price;
  }
  public void setCandle_acc_trade_price(double candle_acc_trade_price) {
    this.candle_acc_trade_price = candle_acc_trade_price;
  }
  public double getCandle_acc_trade_volume() {
    return candle_acc_trade_volume;
  }
  public void setCandle_acc_trade_volume(double candle_acc_trade_volume) {
    this.candle_acc_trade_volume = candle_acc_trade_volume;
  }
  public int getUnit() {
    return unit;
  }
  public void setUnit(int unit) {
    this.unit = unit;
  }
  @Override
  public String toString() {
    return "CandlesMinutesDto [market=" + market + ", candle_date_time_utc=" + candle_date_time_utc + ", candle_date_time_kst=" + candle_date_time_kst
        + ", opening_price=" + opening_price + ", high_price=" + high_price + ", low_price=" + low_price + ", trade_price=" + trade_price
        + ", timestamp=" + timestamp + ", candle_acc_trade_price=" + candle_acc_trade_price + ", candle_acc_trade_volume=" + candle_acc_trade_volume
        + ", unit=" + unit + "]";
  }
}
