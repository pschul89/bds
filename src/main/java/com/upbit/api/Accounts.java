package com.upbit.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;

public class Accounts {

  public List<AccountsDto> get() throws ClientProtocolException, IOException {
    // token
    Algorithm algorithm = Algorithm.HMAC256(StaticConfig.secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", StaticConfig.accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(StaticConfig.serverUrl + "/v1/accounts");
    request.addHeader("Authorization", authenticationToken);

    HttpResponse response = client.execute(request);
    HttpEntity entity = response.getEntity();

    String resJsonString = EntityUtils.toString(entity, "UTF-8");

    Gson gson = new Gson();
    Type type = new TypeToken<ArrayList<AccountsDto>>() {}.getType();
    ArrayList<AccountsDto> data = gson.fromJson(resJsonString, type);

    return data;
  }
}
