package com.upbit.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upbit.config.StaticConfig;
import com.upbit.dto.TickerDto;
import com.upbit.volatilityBreakOut.Main;

public class Ticker {

  public List<TickerDto> get(String market) {
    ArrayList<TickerDto> data = new ArrayList<TickerDto>();

    HashMap<String, String> params = new HashMap<String, String>();
    // multiple "KRW-BTT,KRW-BTC"
    params.put("markets", "KRW-" + market);

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(StaticConfig.serverUrl + "/v1/ticker?" + queryString);
        request.setHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        String resJsonString = EntityUtils.toString(entity, "UTF-8");

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<TickerDto>>() {}.getType();
        data = gson.fromJson(resJsonString, type);
    } catch (IOException e) {
        e.printStackTrace();
    }
    return data;
  }
}
