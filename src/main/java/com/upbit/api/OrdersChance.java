package com.upbit.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upbit.config.StaticConfig;
import com.upbit.dto.OrdersChanceDto;
import com.upbit.volatilityBreakOut.Main;

public class OrdersChance {

  public OrdersChanceDto get() {
    OrdersChanceDto data = null;

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", "KRW-" + Main.market);

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    MessageDigest md = null;
    try {
      md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

    Algorithm algorithm = Algorithm.HMAC256(StaticConfig.secretKey);
    String jwtToken = JWT.create()
            .withClaim("access_key", StaticConfig.accessKey)
            .withClaim("nonce", UUID.randomUUID().toString())
            .withClaim("query_hash", queryHash)
            .withClaim("query_hash_alg", "SHA512")
            .sign(algorithm);

    String authenticationToken = "Bearer " + jwtToken;

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(StaticConfig.serverUrl + "/v1/orders/chance?" + queryString);
        request.setHeader("Content-Type", "application/json");
        request.addHeader("Authorization", authenticationToken);

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        String resJsonString = EntityUtils.toString(entity, "UTF-8");
        System.out.println(resJsonString);

        Gson gson = new Gson();
        Type type = new TypeToken<OrdersChanceDto>() {}.getType();
        data = gson.fromJson(resJsonString, type);
    } catch (IOException e) {
        e.printStackTrace();
    }

    return data;
  }

}
