package com.upbit.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upbit.config.StaticConfig;
import com.upbit.dto.CandlesMinutesDto;

public class CandlesMinutes {

  public ArrayList<CandlesMinutesDto> get(String market, int minuteUnit) {
    ArrayList<CandlesMinutesDto> data = new ArrayList<CandlesMinutesDto>();

    String serverUrl = StaticConfig.serverUrl;

    String requestUrl = serverUrl + "/v1/candles/minutes/" + minuteUnit + "?";

    HashMap<String, String> params = new HashMap<String, String>();
    params.put("market", "KRW-" + market);
//    params.put("to", "yyyy-MM-dd HH:mm:ss");
    params.put("count", "2");

    ArrayList<String> queryElements = new ArrayList<String>();
    for(Map.Entry<String, String> entity : params.entrySet()) {
        queryElements.add(entity.getKey() + "=" + entity.getValue());
    }

    String queryString = String.join("&", queryElements.toArray(new String[0]));

    try {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(requestUrl + queryString);
        request.setHeader("Content-Type", "application/json");

        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();

        String resJsonString = EntityUtils.toString(entity, "UTF-8");

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CandlesMinutesDto>>() {}.getType();
        data = gson.fromJson(resJsonString, type);
    } catch (IOException e) {
        e.printStackTrace();
    }
    return data;
  }

}
