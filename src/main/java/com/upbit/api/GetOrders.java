package com.upbit.api;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upbit.config.StaticConfig;
import com.upbit.dto.GetOrdersDto;

public class GetOrders {

  public List<GetOrdersDto> get() {
    List<GetOrdersDto> data = new ArrayList<GetOrdersDto>();
    try {
      HashMap<String, String> params = new HashMap<>();
      params.put("state", "wait");

//      String[] uuids = {
//          "9ca023a5-851b-4fec-9f0a-48cd83c2eaae"
//      };

      ArrayList<String> queryElements = new ArrayList<>();
      for(Map.Entry<String, String> entity : params.entrySet()) {
          queryElements.add(entity.getKey() + "=" + entity.getValue());
      }

      String queryString = String.join("&", queryElements.toArray(new String[0]));

      MessageDigest md = MessageDigest.getInstance("SHA-512");
      md.update(queryString.getBytes("UTF-8"));

      String queryHash = String.format("%0128x", new BigInteger(1, md.digest()));

      Algorithm algorithm = Algorithm.HMAC256(StaticConfig.secretKey);
      String jwtToken = JWT.create()
              .withClaim("access_key", StaticConfig.accessKey)
              .withClaim("nonce", UUID.randomUUID().toString())
              .withClaim("query_hash", queryHash)
              .withClaim("query_hash_alg", "SHA512")
              .sign(algorithm);

      String authenticationToken = "Bearer " + jwtToken;

      HttpClient client = HttpClientBuilder.create().build();
      HttpGet request = new HttpGet(StaticConfig.serverUrl + "/v1/orders?" + queryString);
      request.setHeader("Content-Type", "application/json");
      request.addHeader("Authorization", authenticationToken);

      HttpResponse response = client.execute(request);
      HttpEntity entity = response.getEntity();

      String resJsonString = EntityUtils.toString(entity, "UTF-8");

      Gson gson = new Gson();
      Type type = new TypeToken<ArrayList<GetOrdersDto>>() {}.getType();
      data = gson.fromJson(resJsonString, type);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return data;
  }
}
