package com.upbit.sc;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class MainSc {

  public static String market = "XRP";
  public static int minuteUnit = 15;
  public static double yield = 3; // 4%

  public static void main(String[] args) {
    if(args.length == 1) {
      market = args[0];
    }
    if(args.length == 2) {
      market = args[0];
      yield = Double.parseDouble(args[1]);
    }

    try {
      JobDetail JobSc = newJob(JobSc.class).withIdentity("JobSc", Scheduler.DEFAULT_GROUP).build();
      Trigger trigger1s = newTrigger().withIdentity("trigger1s", Scheduler.DEFAULT_GROUP).withSchedule(cronSchedule("* * * * * ?")).build();

      SchedulerFactory schedulerFactory = new StdSchedulerFactory();
      Scheduler scheduler = schedulerFactory.getScheduler();
      scheduler.scheduleJob(JobSc, trigger1s);

      scheduler.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
