package com.upbit.sc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.upbit.api.Accounts;
import com.upbit.api.CandlesMinutes;
import com.upbit.api.GetOrders;
import com.upbit.api.Orders;
import com.upbit.api.Ticker;
import com.upbit.config.StaticConfig;
import com.upbit.dto.AccountsDto;
import com.upbit.dto.CandlesMinutesDto;
import com.upbit.dto.GetOrdersDto;
import com.upbit.dto.TickerDto;

public class JobSc implements Job {

  public void execute(JobExecutionContext context) throws JobExecutionException {
    try {
      // 1. 살돈이 있는지 없는지 여부 체크
      List<AccountsDto> accountsList = new Accounts().get();
      AccountsDto accountsKrw = accountsList.stream().filter(f -> "KRW".equals(f.getCurrency())).findAny().orElse(null);

      if(!"0.0".equals(accountsKrw.getLocked())) {
        System.out.println("주문 중 묶여있는 금액/수량이 있습니다.");
      } else {
        AccountsDto accountsMarket = accountsList.stream().filter(f -> MainSc.market.equals(f.getCurrency())).findAny().orElse(null);
        if(accountsMarket == null) {
          StaticConfig.balanceKrw = Double.parseDouble(accountsKrw.getBalance());
          // 매수
          // 30분봉 정보
          ArrayList<CandlesMinutesDto> candlesMinutesList = new CandlesMinutes().get(MainSc.market, MainSc.minuteUnit);

          // 매수가격 책정 (시작가 + (고가-저가) * 0.5)
          List<TickerDto> tickerList = new Ticker().get(MainSc.market);
          double tradePrice = tickerList.get(0).getTrade_price();

          double temp = candlesMinutesList.get(0).getOpening_price() + ((candlesMinutesList.get(1).getHigh_price() - candlesMinutesList.get(1).getLow_price()) * 0.5);
          if(temp < 10) { // 0.01
            StaticConfig.bidAmt = Math.floor(temp * 100) / 100;
          } else if(temp < 100) { // 0.1
            StaticConfig.bidAmt = Math.floor(temp * 10) / 10;
          } else if(temp < 1000) { // 1
            StaticConfig.bidAmt = Math.floor(temp);
          } else if(temp < 10000) { // 5
            temp = Math.floor(temp);
            if(temp % 10 > 5) {
              StaticConfig.bidAmt = temp + (5 - temp % 10);
            } else if(temp % 10 > 0) {
              StaticConfig.bidAmt = temp - temp % 10;
            } else {
              StaticConfig.bidAmt = temp;
            }
          } else if(temp < 100000) { // 10
            StaticConfig.bidAmt = Math.floor(temp / 10) * 10;
          } else if(temp < 1000000) { // 50
            temp = Math.floor(temp / 10) * 10;
            if(temp % 100 > 50) {
              StaticConfig.bidAmt = temp + (50 - temp % 100);
            } else if(temp % 100 > 0) {
              StaticConfig.bidAmt = temp - temp % 100;
            } else {
              StaticConfig.bidAmt = temp;
            }
          } else {
            System.out.println("현재가가 너무 높은 코인입니다. >= 1000000");
            return;
          }



          // 매수량
          // 자산 - 수수료 금액 = 총매수할수 있는 금액
          double susuryo = StaticConfig.balanceKrw * 0.0005;
          double fullBidAmt = StaticConfig.balanceKrw - susuryo;

          int volume = (int) (fullBidAmt / StaticConfig.bidAmt);


          System.out.println("설정된 매수금액: " + StaticConfig.bidAmt + ", 현재가: " + tradePrice + ", 매수신호:" + (tradePrice > StaticConfig.bidAmt));
          if(tradePrice > StaticConfig.bidAmt) {
            new Orders().post("KRW-" + MainSc.market, "bid", volume + "", StaticConfig.bidAmt + "", "limit");
          }
        } else {
          // 시장가 매도
          // wait 주문조회
          List<GetOrdersDto> getOrders = new GetOrders().get();

          List<GetOrdersDto> getOrdersMarket = getOrders.stream().filter(f -> ("KRW-" + MainSc.market).equals(f.getMarket())).collect(Collectors.toList());

          // avg_buy_price;
          if (getOrdersMarket.size() < 1) {
            double temp = Double.parseDouble(accountsMarket.getAvg_buy_price()) * (1 + MainSc.yield / 100);
            if(temp < 10) { // 0.01
              temp = Math.floor(temp * 100) / 100;
            } else if(temp < 100) { // 0.1
              temp = Math.floor(temp * 10) / 10;
            } else if(temp < 1000) { // 1
              temp = Math.floor(temp);
            } else if(temp < 10000) { // 5
              temp = Math.floor(temp);
              if(temp % 10 > 5) {
                temp = temp + (5 - temp % 10);
              } else if(temp % 10 > 0) {
                temp = temp - temp % 10;
              }
            } else if(temp < 100000) { // 10
              temp = Math.floor(temp / 10) * 10;
            } else if(temp < 1000000) { // 50
              temp = Math.floor(temp / 10) * 10;
              if(temp % 100 > 50) {
                temp = temp + (50 - temp % 100);
              } else if(temp % 100 > 0) {
                temp = temp - temp % 100;
              }
            } else {
              System.out.println("현재가가 너무 높은 코인입니다. >= 1000000");
              return;
            }
            new Orders().post("KRW-" + MainSc.market, "ask", accountsMarket.getBalance(), String.valueOf(temp), "limit");
          } else {
            return;
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
